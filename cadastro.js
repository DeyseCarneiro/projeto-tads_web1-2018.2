var form =  document.getElementById("cadastro");
form.addEventListener("submit", function(e){
    e.preventDefault();
    var obj = {
        nome : document.getElementById("name").value,
        username : document.getElementById("username").value,
        password : document.getElementById("senha").value
    }
    var json = JSON.stringify(obj);
    var xhr = new XMLHttpRequest();
    xhr.open("POST",'http://www.henriquesantos.pro.br:8080/api/trello/users/new', true);
    xhr.setRequestHeader("Content-type", "application/json");
    xhr.onreadystatechange = function(){
        if(xhr.readyState == XMLHttpRequest.DONE && xhr.status == 200){
            console.log(xhr.responseText);
        }
    }
    xhr.send(json);
});